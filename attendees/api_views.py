from django.http import JsonResponse

from events.models import Conference
from common.json import ModelEncoder

from .models import Attendee

from django.views.decorators.http import require_http_methods

import json


class AttendeeListEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "name",
    ]


class AttendeeDetailEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "email",
        "name",
        "company_name",
        "created",
    ]

    def get_extra_data(self, o):
        return {
            "conference_name": o.conference.name,
            "conference_href": o.conference.get_api_url(),
        }


@require_http_methods(["GET", "POST"])
def api_list_attendees(request, conference_id):
    if request.method == "GET":
        attendee = Attendee.objects.filter(conference=conference_id)
        return JsonResponse(
            {"attendee": attendee},
            encoder=AttendeeListEncoder,
        )
    else:
        content = json.loads(request.body)

        # Get conference object and put it in content Dict
        try:
            conference = Conference.objects.get(id=conference_id)
            content["conference"] = conference
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Conference ID"}, status=400
            )

        attendee = Attendee.objects.create(**content)
        return JsonResponse(
            attendee, encoder=AttendeeDetailEncoder, safe=False
        )


# ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
# This looks similar to what you have already done previously.
# The big difference is that you don't use the submitted content
# to look up the Conference. Instead, you use the function's conference_id
# parameter.


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_attendee(request, pk):
    if request.method == "GET":
        attendee = Attendee.objects.get(id=pk)
        return JsonResponse(
            attendee,
            encoder=AttendeeDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Attendee.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        Attendee.objects.filter(id=pk).update(**content)

        attendee = Attendee.objects.get(id=pk)
        return JsonResponse(
            attendee,
            encoder=AttendeeDetailEncoder,
            safe=False,
        )
